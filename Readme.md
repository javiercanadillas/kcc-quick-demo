# Instructions

This demo is based on the [GCP Config Connector quickstart](https://cloud.google.com/config-connector/docs/how-to/getting-started). If you run the script, it will allow you to focus on demoing the Config Connector relevant parts as it will configure all the necessary things in advance. The demo script will also focus in a different resource, and hints at future integrations with config as data tools as [Kpt](https://googlecontainertools.github.io/kpt/).

## Run the script

You need a GCP project, the Cloud SDK installed, and your project ID, zone and regions set up as bash variables in the `setup.sh` file. Ready? Run:

```bash
./setup.sh install
```

For additional help running the script, try:

```bash
./setup --help
```

### What the script did

**KCC is a Kubernetes Operator supplied by Google**. There are several ways to install it, for the purposes of the demo will be installing it **through the GKE add-ons mechanism**, that although less frequently updated, offers a more convenient installation.

It requires enabling **Workload Identity** and Kubernetes Engine Monitoring. Workload Identity will give us here the capability of letting a controller manager to perform actions on GCP resources by giving Kubernetes Service Accounts (KSAs) true IAM roles in the form of `serviceAccount:PROJECT_ID.svc.id.goog[K8S_NAMESPACE/KSA_NAME]` 

So as of today, to manage our resources the Kubernetes way, **we need a Kubernetes cluster to host it**. In this demo we have a GKE cluster with the Config Connector (KCC from now on) enabled. The cluster is configured to be in the Rapid release channel, it's a zonal cluster, and has the KCC add on enabled and configured.

KCC creates and manages GCP resources by authenticating with an IAM service account and using GKE's Workload Identity to bind IAM SAs with Kubernetes service accounts. So the demo has created a service account named `sa-kcc-demo`and has given that account owner permissions on the specific project. 

Then, we've given the CNRM controller manager Kubernetes Service Account elevated permissions to impersonate our `sa-kcc-demo` service account that it project owner through a policy binding. Remember that for GCP's resource manager, SA's are both an identity **and** a resource.

Once the `setup.sh`script has the KCC operator installed in the cluster through the GKE, add-on, it also configures the  KCC CustomResource through the operator (see the file  `configconnector.yaml`). The only thing required in the configuration that is specific for your installation is service account to be used by the KCC controller manager.

The last step is adding a KCC specific annotation to the namespace to wich we'll be requesting the GCP resources to be created. Because this is a demo, we won't be using a specific one and we'll make use of the namespace `default` instead.

Read on to test all this with a specific GCP resource.

## Demoing

Test that everything went fine:

```bash

```

### Discovering available Google Cloud resources

To see what kinds of Google Cloud resources you can create with Config Connector, run:

```bash
kubectl get crds --selector cnrm.cloud.google.com/managed-by-kcc=true
```

The output is a list of all the resources your version of Config Connector can create and manage. For example, you can view the API description for the ComputeInstances resource with `kubectl describe`:

```bash
!! | grep computeinstances
kubectl describe crd computeinstaces.compute.cnrm.cloud.google.com
```

We'll be focusing on creating a GCE instance from now on. You can also see information on available resources in [Config Connector resources](https://cloud.google.com/config-connector/docs/reference/overview).

### Enabling the Compute Service

First, let's use KCC to enable the GCE API in our project. You can do that by applying the `enable-compute.yaml` yaml configuration file to your cluster:

```bash
more enable-compute.yaml
kubectl apply -f enable-compute.yaml
```

### Creating the instance

Now, let's create a new instance in our project defined by the file `compute-instance.yaml`. First, have a look at the file with your editor. Then, request the bucket creation:

```bash
kubectl apply -f compute-instance.yaml
```

This file complies with the [Custom Resource Definition properties for ComputeInstance](https://cloud.google.com/config-connector/docs/reference/resource-docs/compute/computeinstance) that you can find in the documentation.

### Describing the resource

You can view all your Compute Instance using kubectl:

```bash
kubectl get ComputeInstance
kubectl describe ComputeInstance kcc-vm
```

You can also use `gcloud compute instances list` or the Cloud Console to check that the bucket was created.

### Updating the resource

You can update metadata on your resources by updating your YAML file and re-applying it with kubectl. To update the metadata on the storage bucket you created earlier, apply a new yaml file that adds protection against deletion to the VM:

```bash
kubectl apply -f compute-instance-protected.yaml
```

### Destroying the resource

```bash
kubectl delete ComputeInstance kcc-vm
```

## Extra: Using kpt with Config Connector

Let's see now how we could have customized the definitions we've got here. Go to the following directory:

```bash
cd kcc-kpt
```

First, show a the three yaml definition files that we have in an editor:
- `configconnector.yaml`
- `compute-instance.yaml`
- `compute-instance-protected.yaml`

Note the annotations in the files that allow us to customize the file using Kpt setters. Now, with an editor, open the `Kptfile` and see how we're defining setters there for the parameters we want to customize/patch that correspond to the annotations seen as comments in the yaml file. You can see the setters and the values applied by asking `kpt`:

```bash
kpt cfg list-setters .
```

Then, you can customize the setters by specifying new values:
```bash
kpt cfg set . project-id javiercm-main-demos
kpt cfg set . instanceName kpt-rocks
kpt cfg set . googleServiceAccount sa-kcc-demo@javiercm-main-demos.iam.gserviceaccount.com
```

Check that the values have been updated by both exploring the files and doing:

```bash
kpt cfg list-setters .
```

Finally, you could apply the objects by using `kubectl`:

```bash
kubectl apply -f .
```

## Resetting to pre-demo status

Just re-run the script with the `destroy` option:

```bash
./setup.sh destroy
```

## Additional info

- [Installing KCC in non-GKE clusters](https://cloud.google.com/config-connector/docs/how-to/install-other-kubernetes)
- [Creating resource references](https://cloud.google.com/config-connector/docs/how-to/creating-resource-references) (managing dependencies)
- [Cloud Foundation Toolkit](https://github.com/GoogleCloudPlatform/cloud-foundation-toolkit)
- [Kpt main page](https://googlecontainertools.github.io/kpt/)
 