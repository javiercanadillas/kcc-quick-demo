#!/usr/bin/env bash

readonly SCRIPT_NAME="${0##*/}"
readonly SCRIPT_DIR=$(cd "$(dirname "$0")" && pwd)
readonly ARTIFACTS_DIR=artifacts
green=$(tput setaf 2)
red=$(tput setaf 1)
reset=$(tput sgr0)
PROJECT_ID='javiercm-main-demos'
CLUSTER_NAME='kcc'
CHANNEL='rapid'
ZONE='europe-west1-b'
REGION='europe-west1'
SERVICE_ACCOUNT_NAME='sa-kcc-demo'
NAMESPACE='config-connector'
INSTANCE_NAME='kcc-vm'
CONFIG_FILE_NAME=configconnector.yaml
ENABLEMENT_FILE_NAME=enable-compute.yaml
COMPUTE_FILE_NAME=compute-instance.yaml
COMPUTE_FILE_NAME_PROTECTED=compute-instance-protected.yaml

info() {
  echo "${green}${SCRIPT_NAME}${reset}: ${1}" >&2
}

replace_in_file() {
  # -i '' option in sed as OS X required that a backup file is specified
  # Linux does not use the space, so let's detect the OS
  local -r OS=$(uname)
  if [ "$OS" = 'Darwin' ]; then
    # for MacOS
    sed -i '' -e "s/${1}/${2}/g"
  else
    # for Linux and Windows
    sed -i'' -e "s/${1}/${2}/g"
  fi
}

config::pwd_script_dir() {
  info "Setting working directory at ${SCRIPT_DIR}"
  pushd "${SCRIPT_DIR}" > /dev/null || return
}

initial_setup() {
  info "Setting up project variables"
  gcloud config set project "${PROJECT_ID}" 2>/dev/null
  gcloud config set compute/zone "${ZONE}" 2>/dev/null
  gcloud config set compute/region "${REGION}" 2>/dev/null
  info "Enabling APIs"
  gcloud services enable cloudresourcemanager.googleapis.com
}

create_cluster() {
  info "Creating the GKE cluster"
  gcloud container clusters create "${CLUSTER_NAME}" \
    --release-channel "${CHANNEL}" \
    --addons ConfigConnector \
    --workload-pool="${PROJECT_ID}".svc.id.goog \
    --enable-stackdriver-kubernetes
}

rename_context() {
  info "Renaming context for cluster ${CLUSTER_NAME} and enabling it"
  kubectx "${CLUSTER_NAME}"=gke_"${PROJECT_ID}"_"${ZONE}"_"${CLUSTER_NAME}"
  kubectx "${CLUSTER_NAME}"
}

setup_sa() {
  info "Setting up the service account"
  gcloud iam service-accounts create "${SERVICE_ACCOUNT_NAME}"
  # Give the service account elevated permissions on your project
  gcloud projects add-iam-policy-binding "${PROJECT_ID}" \
    --member="serviceAccount:${SERVICE_ACCOUNT_NAME}@${PROJECT_ID}.iam.gserviceaccount.com" \
    --role="roles/owner"
  # We allow the CNRM Controller Manager Kubernetes Service account to impersonate our previous Service Account
  gcloud iam service-accounts add-iam-policy-binding \
    "${SERVICE_ACCOUNT_NAME}@${PROJECT_ID}.iam.gserviceaccount.com" \
    --member="serviceAccount:${PROJECT_ID}.svc.id.goog[cnrm-system/cnrm-controller-manager]" \
    --role="roles/iam.workloadIdentityUser"
}

setup_kcc() {
  info "Setting up KCC in the GKE cluster"
  # Modify kcc configuration file to match our SA and Project
  sed \
    -e "s/SERVICE_ACCOUNT_NAME/${SERVICE_ACCOUNT_NAME}/g" \
    -e "s/PROJECT_ID/${PROJECT_ID}/g" \
    "${SCRIPT_DIR}/${ARTIFACTS_DIR}/${CONFIG_FILE_NAME}" \
    >"${SCRIPT_DIR}/${CONFIG_FILE_NAME}"
  kubectl apply -f "${SCRIPT_DIR}/${CONFIG_FILE_NAME}"
  # Create and annotate namespace so resources are created there
  info "Creating namespace ${NAMESPACE} and annotating it for CNRM"
  kubectl create namespace "${NAMESPACE}"
  kubectl annotate namespace \
    "${NAMESPACE}" cnrm.cloud.google.com/project-id="${PROJECT_ID}"
  info "Setting ${NAMESPACE} as default in the current context"
  kubectl config set-context --current --namespace "${NAMESPACE}"
}

setup_resources() {
  info "Customizing artifacts and leaving them in ${SCRIPT_DIR}"
  # Leaves final resource files in script directory
  cp "${SCRIPT_DIR}/${ARTIFACTS_DIR}/${ENABLEMENT_FILE_NAME}" "${SCRIPT_DIR}/${ENABLEMENT_FILE_NAME}"
  FILENAMES=("${COMPUTE_FILE_NAME}" "${COMPUTE_FILE_NAME_PROTECTED}")
  for FILENAME in "${FILENAMES[@]}"; do
    sed -e "s/INSTANCE_NAME/${INSTANCE_NAME}/g" \
    -e "s/PROJECT_ID/${PROJECT_ID}/g" \
    "${SCRIPT_DIR}/${ARTIFACTS_DIR}/${FILENAME}"\
    >"${SCRIPT_DIR}/${FILENAME}"
  done
}

main::install() {
  initial_setup
  create_cluster
  rename_context
  setup_sa
  setup_kcc
  setup_resources
}

main::destroy() {
  info "Destroying cluster ${CLUSTER_NAME} and associated configuration and installation objects"
  config::pwd_script_dir
  # Remove cluster
  gcloud container clusters delete "${CLUSTER_NAME}" --zone "${ZONE}" --quiet
  # Remove IAM permissions
  gcloud iam service-accounts remove-iam-policy-binding \
    "${SERVICE_ACCOUNT_NAME}@${PROJECT_ID}.iam.gserviceaccount.com" \
    --member="serviceAccount:${PROJECT_ID}.svc.id.goog[cnrm-system/cnrm-controller-manager]" \
    --role="roles/iam.workloadIdentityUser"
  gcloud projects remove-iam-policy-binding "${PROJECT_ID}" \
    --member="serviceAccount:${SERVICE_ACCOUNT_NAME}@${PROJECT_ID}.iam.gserviceaccount.com" \
    --role="roles/owner"
  gcloud iam service-accounts delete "${SERVICE_ACCOUNT_NAME}@${PROJECT_ID}.iam.gserviceaccount.com" --quiet 
  
  # Delete local files
  rm -f ./*.yaml
}

main::usage() {
  cat << EOF
Usage: ${SCRIPT_NAME} [COMMAND] [OPTIONs]...
Creates/destroys GKE cluster on GCP with ASM deployed in it.

COMMAND:
  install                           Installs the GKE cluster
  destroy                           Destroys all created infrastructure,
                                    cleaning up the project

OPTIONS:
  -p|--project-id <PROJECT ID>      GCP project ID where the services will
                                    be deployed
  -c|--cluster-name <CLUSTER NAME>  Name of the GKE cluster
  -z|--cluster-zone <CLUSTER ZONE>  Location of the GKE cluster
  -n|--channel <CLUSTER CHANNEL>    Cluster release channel (rapid | regular | stable)

FLAGS:
  -d|--debug                        Enable script debugging
  -h|--help                         Shows this help text

EXAMPLES:
The following invocation installs GKE and sets up additional demo artifacts
  $> ${SCRIPT_NAME} install
EOF
}

# This function checks <command -option|--option argument>
# If no argument is supplied, it fails
arg_required() {
  if [[ ! "${2:-}" || "${2:0:1}" = '-' ]]; then
    echo "Option ${1} requires an argument."
  fi
}

parse_command() {
  # shellcheck disable=SC2064
  trap "$(shopt -p nocasematch)" RETURN
  shopt -s nocasematch

  case "${1}" in
  install)
    if [ -z "${PROJECT_ID}" ]; then
      echo "${red}Error${reset}: you must set a GCP project. Try doing \'gcloud config set project <projectname>\' before running the script."
    else
      main::install
    fi
    ;;
  destroy | uninstall | remove)
    main::destroy
    ;;
  *) #Catch empty or not listed command
    if [ -z "${1}" ]; then
      echo "Error: command not supplied, try again."
    else
      echo "Error: invalid command \"${1}\""
      echo ""
      main::usage
    fi
    ;;
  esac
  exit
}

parse_args() {
  # shellcheck disable=SC2064
  trap "$(shopt -p nocasematch)" RETURN
  shopt -s nocasematch

  while [[ $# != 0 ]]; do
    case "${1}" in
    -p | --project_id)
      arg_required "${@}"
      PROJECT_ID="${2}"
      shift 2
      ;;
    -c | --cluster_name | --cluster-name)
      arg_required "${@}"
      CLUSTER_NAME="${2}"
      shift 2
      ;;
    -n | --channel)
      arg_required "${@}"
      CHANNEL="${2}"
      shift 2
      ;;
    -z | --cluster_zone | --cluster-zone)
      arg_required "${@}"
      ZONE="${2}"
      shift 2
      ;;
    -d | --debug)
      # This is imperfect, because the script has already processed some lines, including default
      # variable values, but it's better than nothing
      set -x
      shift 1
      ;;
    -h | --help)
      main::usage
      exit
      ;;
    --* | -*=)
      echo "Error: unsupported flag ${1}" >&2
      exit 1
      ;;
    *)
      PARAMS+=("${1}")
      shift
      ;;
    esac
  done
}

main() {
  parse_args "${@}"
  parse_command "${PARAMS[*]}"
}

main "${@}"
